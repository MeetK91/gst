package gst.controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Hetarth
 */
public class DashboardController implements Initializable {

    @FXML
    private AnchorPane root;

    @FXML
    private BorderPane borderPane;

    @FXML
    private MenuItem menuItemClient;

    @FXML
    private MenuItem menuItemCompare;
    
    @FXML
    private MenuItem menuItemExit;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        menuItemClient.setOnAction(event -> {
            try {
                borderPane.setCenter(FXMLLoader.load(getClass().getResource("/gst/view/Client.fxml")));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        menuItemCompare.setOnAction(event -> {
            try {
                borderPane.setCenter(FXMLLoader.load(getClass().getResource("/gst/view/ColumnChoice.fxml")));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        
        menuItemExit.setOnAction(event -> {
            try {
                System.exit(0);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        
        try {
            borderPane.setCenter(FXMLLoader.load(getClass().getResource("/gst/view/ColumnChoice.fxml")));
        } catch (IOException ex) {
            Logger.getLogger(DashboardController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}