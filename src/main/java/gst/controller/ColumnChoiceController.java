/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javafx.beans.Observable;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.util.converter.IntegerStringConverter;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;

import gst.dao.ClientDao;
import gst.model.Client;
import gst.model.CompareModel;
import gst.model.ExcelColumn;
import gst.model.FinalTableModel;
import gst.service.ExcelFileService;
import gst.util.AlertHelper;
import gst.util.FileHelper;

/**
 *
 * @author mjkulkarni
 */
public class ColumnChoiceController implements Initializable {

    @FXML
    AnchorPane root;

    @FXML
    ComboBox<Client> comboClient;

    @FXML
    TextField txtFixedFilePath;

    @FXML
    Button btnCustomFileImport;

    @FXML
    Button btnProcess;

    @FXML
    TextField txtCustomFilePath;

    @FXML
    Button btnFixedFileImport;

    @FXML
    Button btnReset;

    @FXML
    TableView<ExcelColumn> tblFixedColumns;

    @FXML
    TableColumn<ExcelColumn, String> fixedColumnName;

    @FXML
    TableColumn<ExcelColumn, Integer> fixedColumnIndex;

    @FXML
    TableView<ExcelColumn> tblCustomColumns;

    @FXML
    TableColumn<ExcelColumn, String> customColumnName;

    @FXML
    TableColumn<ExcelColumn, Integer> customColumnIndex;

    @FXML
    TableView<FinalTableModel> tblFinalColumns;

    @FXML
    TableColumn<FinalTableModel, String> finalFixedColumn;

    @FXML
    TableColumn<FinalTableModel, String> finalCustomColumn;

    @FXML
    TableColumn<FinalTableModel, Boolean> finalComparison;

    private File fixedFile, customFile;

    private List<ExcelColumn> fixedFileColumns, customFileColumns;

    private List<CompareModel> compareColumnList;

    private ObservableList<FinalTableModel> observableList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btnFixedFileImport.setOnAction(event -> {
            handleFixedFileImport();
        });

        btnCustomFileImport.setOnAction(event -> {
            handleCustomFileImport();
        });

        btnReset.setOnAction(event -> {
            resetAllData();
        });

        comboClient.setOnAction(event -> {
            resetCustomAndFixedFileData();
        });

        btnProcess.setOnAction(event -> {
            handleProcessFiles();
        });

        /*
        Creating an observable list to populate checkbox column in final compare table.
        This is required because setting TableColumn's cell factory to CheckboxTableCell stops firing of
        onEditStart and onEditCommit events of that TableColumn.
         */
        observableList = FXCollections.observableArrayList((FinalTableModel param) -> new Observable[]{param.compareProperty()});

        /*
        Listening for change in checkbox's state by adding a change listener on the ObservableList, since
        TableColumn's onEditStart and onEditCommit events are not available. This list will then become the 
        datasource for Table containing checkbox columns.
         */
        observableList.addListener((ListChangeListener.Change<? extends FinalTableModel> c) -> {
            while (c.next()) {
                if (c.wasUpdated()) {
                    FinalTableModel f = observableList.get(c.getFrom());
                    System.out.println("Update: " + f.getCustomColumn() + " - " + f.compareProperty().get());
                    updateComparisonList();
                }
            }
        });
        
        compareColumnList = new ArrayList<>();

        setupTables();
        loadData();

    }

    private void handleProcessFiles() {

        if (fixedFile == null || customFile == null) {
            AlertHelper.showError("Please select all files!!");
            resetMappingData();
            return;
        }

        try {
            Workbook diffBook = ExcelFileService.evaluate(compareColumnList, ExcelFileService.getWorkbook(fixedFile, FileHelper.EXTENSION_XLSX), ExcelFileService.getWorkbook(customFile, FileHelper.EXTENSION_XLSX), FileHelper.EXTENSION_XLSX);
            FileHelper.saveWorkbook(diffBook, FileHelper.EXTENSION_XLSX, root.getScene().getWindow());
            AlertHelper.showInfo("File saved!");
        } catch (Exception e) {
            AlertHelper.showError(e.getMessage());
            return;
        }
    }

    private void handleFixedFileImport() {

        /**
         * Fixed File's data is saved client-wise. Hence, user must have
         * selected client before uploading Fixed File.
         */
        if (comboClient.getSelectionModel().getSelectedItem() == null) {
            AlertHelper.showError("Please select client first!");
            return;
        }

        try {
            fixedFile = FileHelper.chooseFile(root.getScene().getWindow());
        } catch (FileNotFoundException fnfe) {
            AlertHelper.showInfo(fnfe.getMessage());
            resetFixedFileData();
            return;
        }

        txtFixedFilePath.setText(fixedFile.getAbsolutePath());

        try {
            fixedFileColumns = ExcelFileService.processExcelColumns(fixedFile);
            tblFixedColumns.setItems(FXCollections.observableArrayList(fixedFileColumns));
        } catch (InvalidFormatException | IOException e) {
            AlertHelper.showError(e.getMessage());
            resetFixedFileData();
        }

        return;

    }

    private void handleCustomFileImport() {

        /*
         * Data of Custom File is to be compared with data of Fixed File. Hence, user must 
         * upload Fixed File before uploading Custom File.
         */
        if (txtFixedFilePath.getText() == null || txtFixedFilePath.getText().isEmpty()) {
            AlertHelper.showError("Please select fixed format file first!");
            resetCustomFileData();
            return;
        }

        try {
            customFile = FileHelper.chooseFile(root.getScene().getWindow());
        } catch (FileNotFoundException fnfe) {
            AlertHelper.showInfo(fnfe.getMessage());
            resetCustomFileData();
            return;
        }

        try {

            /*
                In order to compare data between Fixed and Custom files, we will compare data 
                in corresponding columns. Each Custom File column will be mapped to one column in 
                Fixed File. Hence, column count of Custom File must be equal to that of Fixed File.
             */
            if (!ExcelFileService.isColumnCountEqual(fixedFile, customFile)) {
                AlertHelper.showError("Both files should have same number of columns!");
                resetCustomFileData();
                return;
            }

            customFileColumns = ExcelFileService.processExcelColumns(customFile);
            txtCustomFilePath.setText(customFile.getAbsolutePath());
            tblCustomColumns.setItems(FXCollections.observableArrayList(customFileColumns));
            populateCustomColumnIndexDropdown();
            setupFinalTableData();

        } catch (InvalidFormatException | IOException e) {
            AlertHelper.showError(e.getMessage());
            resetCustomFileData();
        }
    }

    private void populateCustomColumnIndexDropdown() {
        ObservableList<Integer> options = FXCollections.observableArrayList();

        for (ExcelColumn c : fixedFileColumns) {
            options.add(c.getColumnIndex());
        }

        customColumnIndex.setCellFactory(ComboBoxTableCell.forTableColumn(new IntegerStringConverter(), options));
        customColumnIndex.setCellValueFactory(new PropertyValueFactory<>("columnIndex"));
        customColumnIndex.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<ExcelColumn, Integer>>() {

            @Override
            public void handle(TableColumn.CellEditEvent<ExcelColumn, Integer> event) {
                refreshFinalCompareTable(event);
            }

        });
    }

    private void setupFinalTableData() {

        List<ExcelColumn> mapping = tblCustomColumns.getItems();
        List<FinalTableModel> finalTableData = new ArrayList<>();

        for (int i = 0; i < mapping.size(); i++) {

            ExcelColumn c = mapping.get(i);
            ExcelColumn f = fixedFileColumns.get(c.getColumnIndex() - 1);
            FinalTableModel finalRow = new FinalTableModel();
            finalRow.setFixedColumn(f.getColumnName());
            finalRow.setCustomColumn(c.getColumnName());
            finalRow.setCompare(new SimpleBooleanProperty(false));
            finalTableData.add(finalRow);
        }

        observableList.clear();
        observableList.addAll(finalTableData);
        tblFinalColumns.setItems(observableList);
        tblFinalColumns.setEditable(true);
        finalFixedColumn.setEditable(false);
        finalCustomColumn.setEditable(false);
        finalComparison.setEditable(true);

        updateComparisonList();
    }

    private void refreshFinalCompareTable(TableColumn.CellEditEvent<ExcelColumn, Integer> event) {

        if (fixedFile == null) {
            AlertHelper.showError("Fixed file not selected!");
            return;
        }

        List<FinalTableModel> finalTableData = new ArrayList<>();
        finalTableData.addAll(tblFinalColumns.getItems());

        System.out.println("Refresh event position: " + event.getTablePosition().getRow());
        for (int i = 0; i < finalTableData.size(); i++) {

            FinalTableModel finalRow = finalTableData.get(i);

            finalRow.setCompare(new SimpleBooleanProperty(false));

            if (finalRow.getFixedColumn().equals(event.getRowValue().getColumnName())) {
                finalRow.setCustomColumn(fixedFileColumns.get(event.getNewValue() - 1).getColumnName());
                System.out.println("Refresh event index: " + i);
            }

            finalTableData.set(i, finalRow);

        }

        observableList.clear();
        observableList.addAll(finalTableData);
        tblFinalColumns.setItems(observableList);
        tblFinalColumns.refresh();

    }

    private void resetCustomFileData() {
        txtCustomFilePath.setText(null);
        tblCustomColumns.setItems(null);
        customFile = null;
    }

    private void resetFixedFileData() {
        txtFixedFilePath.setText(null);
        tblFixedColumns.setItems(null);
        fixedFile = null;
    }

    private void resetCustomAndFixedFileData() {
        resetCustomFileData();
        resetFixedFileData();
        resetMappingData();
    }

    private void resetMappingData() {
        compareColumnList.clear();
        observableList.clear();
        tblFinalColumns.setItems(null);
    }

    private void resetAllData() {
        comboClient.getSelectionModel().clearSelection();
        resetCustomFileData();
        resetFixedFileData();
        resetMappingData();
    }

    private void setupTables() {
        fixedColumnName.setCellValueFactory(new PropertyValueFactory<>("columnName"));
        fixedColumnIndex.setCellValueFactory(new PropertyValueFactory<>("columnIndex"));
        customColumnName.setCellValueFactory(new PropertyValueFactory<>("columnName"));
        tblCustomColumns.setEditable(true);
        customColumnName.setEditable(false);
        customColumnIndex.setEditable(true);

        finalFixedColumn.setCellValueFactory(new PropertyValueFactory<>("fixedColumn"));
        finalCustomColumn.setCellValueFactory(new PropertyValueFactory<>("customColumn"));
        finalComparison.setCellFactory(CheckBoxTableCell.forTableColumn(finalComparison));
        finalComparison.setCellValueFactory(new PropertyValueFactory<FinalTableModel, Boolean>("compare"));
    }

    private void loadData() {
        try {
            comboClient.setItems(FXCollections.observableArrayList(ClientDao.fetchAll()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateComparisonList() {
        compareColumnList = new ArrayList<>();
        List<ExcelColumn> fixedList = tblFixedColumns.getItems();
        List<ExcelColumn> customList = tblCustomColumns.getItems();
        ObservableList<FinalTableModel> observableList = tblFinalColumns.getItems();

        for (FinalTableModel f : observableList) {

            int i = -1;
            int j = -1;
            boolean compareVal = f.getCompare().get();
            for (ExcelColumn c : fixedList) {
                if (c.getColumnName().equals(f.getFixedColumn())) {
                    i = c.getColumnIndex() - 1;
                    break;
                }
            }

            for (ExcelColumn c : customList) {
                if (c.getColumnName().equals(f.getCustomColumn())) {
                    j = c.getColumnIndex() - 1;
                    break;
                }
            }
            compareColumnList.add(new CompareModel(i, j, compareVal));

        }
    }

}