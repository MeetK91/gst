/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.controller;

import java.net.URL;
import java.util.ResourceBundle;

import gst.dao.EmployeeDao;
import gst.model.Employee;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 *
 * @author Hetarth
 */
public class EmployeeController implements Initializable{
    
    @FXML
    private TextField txtFName,txtLName,txtEmail;

    @FXML
    private TableView<Employee> tblEmployee;

    @FXML
    private TableColumn<Employee, String> colFName,colLName,colEmail;

    @FXML
    private Button btnAdd,btnEdit,btnDelete;

    @FXML
    public void insert(ActionEvent event){
        EmployeeDao.insert(txtFName.getText(),txtLName.getText(),txtEmail.getText());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btnAdd.setOnAction(event->{
            insert(event);
        });
    
    }
    
        
}
