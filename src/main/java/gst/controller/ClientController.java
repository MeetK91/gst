package gst.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;

import gst.dao.ClientDao;
import gst.model.Client;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Hetarth
 */
public class ClientController implements Initializable {

    @FXML
    private TextField txtClient;

    @FXML
    private Button btnAdd, btnEdit, btnDelete;

    @FXML
    private TableView<Client> tableClient;

    @FXML
    private TableColumn<Client, Number> colClinetId;

    @FXML
    private TableColumn<Client, String> colClientName;

    @FXML
    private TableColumn<Client, Boolean> colActive;

    @FXML
    private CheckBox chkActive;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        btnAdd.setOnAction(event -> {
            insert(event);
        });

        setupTable();
        loadData();
    }

    public void insert(ActionEvent event) {

        Client client = new Client();
        client.setClientName(txtClient.getText());
        client.setIsActive(chkActive.isSelected());

        ClientDao.insert(client);
    }

    private void setupTable() {
        colClinetId.setCellValueFactory(new PropertyValueFactory<>("clientId"));
        colClientName.setCellValueFactory(new PropertyValueFactory<>("clientName"));
        colActive.setCellValueFactory(new PropertyValueFactory<>("isActive"));
    }

    public void loadData() {
        System.out.println("In loadData");
        try {
            tableClient.setItems(FXCollections.observableArrayList(ClientDao.fetchAll()));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}