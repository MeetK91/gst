/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import gst.model.CompareModel;
import gst.model.ExcelColumn;
import gst.util.FileHelper;

import org.apache.poi.ss.usermodel.FormulaEvaluator;

/**
 *
 * @author mjkulkarni
 */
public class ExcelFileService {

    public static Workbook evaluate(List<CompareModel> compareColumnList, File fixedFile, File customFile, int EXPORT_FILE_TYPE) throws InvalidFormatException, IOException {

        switch (EXPORT_FILE_TYPE) {

            case FileHelper.EXTENSION_XLSX:
                return evaluateXLSX(compareColumnList, getWorkbook(fixedFile, FileHelper.EXTENSION_XLSX), getWorkbook(customFile, FileHelper.EXTENSION_XLSX));

            case FileHelper.EXTENSION_XLS:
                return evaluateXLS(compareColumnList, getWorkbook(fixedFile, FileHelper.EXTENSION_XLS), getWorkbook(customFile, FileHelper.EXTENSION_XLS));

            default:
                throw new InvalidFormatException("Incorrect file type argument!");

        }

    }

    public static Workbook evaluate(List<CompareModel> compareColumnList, Workbook fixedFile, Workbook customFile, int EXPORT_FILE_TYPE) throws InvalidFormatException {

        switch (EXPORT_FILE_TYPE) {

            case FileHelper.EXTENSION_XLSX:
                return evaluateXLSX(compareColumnList, fixedFile, customFile);

            case FileHelper.EXTENSION_XLS:
                return evaluateXLS(compareColumnList, fixedFile, customFile);

            default:
                throw new InvalidFormatException("Incorrect file type argument!");

        }

    }

    private static Workbook evaluateXLSX(List<CompareModel> compareColumnList, Workbook fixedFile, Workbook customFile) throws IllegalArgumentException {

        int excessRowMode = 0;
        Sheet fixedSheet, customSheet;
        Row fixedRow, customRow;
        Cell fixedCell, customCell;
        List<Row> unmatchedRows = new ArrayList<>();

        fixedSheet = fixedFile.getSheetAt(0);
        customSheet = customFile.getSheetAt(0);

        /*
        Handling the case where both files contain different number of rows.
         */
        int rowCount = 0;
        boolean isMatch = false;

        if (fixedSheet.getPhysicalNumberOfRows() == customSheet.getPhysicalNumberOfRows()) {
            rowCount = fixedSheet.getPhysicalNumberOfRows();
        } else {
            if (fixedSheet.getPhysicalNumberOfRows() < customSheet.getPhysicalNumberOfRows()) {
                rowCount = fixedSheet.getPhysicalNumberOfRows();
                excessRowMode = 1;
            } else {
                rowCount = customSheet.getPhysicalNumberOfRows();
                excessRowMode = 2;
            }
        }

        unmatchedRows.add(fixedSheet.getRow(0));

        customFile:
        for (int i = 1; i < rowCount; i++) {

            customRow = customSheet.getRow(i);
            isMatch = false;

            fixedFile:
            for (int k = 1; k < rowCount; k++) {

                fixedRow = fixedSheet.getRow(k);

                for (int j = 0; j < compareColumnList.size(); j++) {

                    CompareModel c = compareColumnList.get(j);
                    if (c.isCompare()) {
                        fixedCell = fixedRow.getCell(c.getFixedIndex());
                        customCell = customRow.getCell(c.getCustomIndex());

                        FormulaEvaluator evaluator = fixedFile.getCreationHelper().createFormulaEvaluator();

                        if (evaluator.evaluateInCell(fixedCell).getCellType().equals(evaluator.evaluateInCell(customCell).getCellType())) {

                            switch (evaluator.evaluateInCell(fixedCell).getCellType()) {

                                case BLANK:
                                case STRING:
                                    if (fixedCell.getStringCellValue().equals(customCell.getStringCellValue())) {
                                        isMatch = true;
                                    } else {
                                        isMatch = false;
                                        continue fixedFile;
                                    }
                                    break;

                                case NUMERIC:
                                    if (fixedCell.getNumericCellValue() == customCell.getNumericCellValue()) {
                                        isMatch = true;
                                    } else {
                                        isMatch = false;
                                        continue fixedFile;
                                    }
                                    break;

                                case BOOLEAN:
                                    if (fixedCell.getBooleanCellValue() == customCell.getBooleanCellValue()) {
                                        isMatch = true;
                                    } else {
                                        isMatch = false;
                                        continue fixedFile;
                                    }
                                    break;

                                case FORMULA:
                                    throw new IllegalArgumentException("Recursive formula cells not yet supported!");
                                //break;
                                default:
                            }

                        } else {
                            isMatch = false;
                        }

                    }
                }

                if (isMatch) {
                    continue customFile;
                }

            }

            if (!isMatch) {
                unmatchedRows.add(customRow);
            }
        }

        /*
        If row count in both sheets is unequal, then add remaining rows from either file to list of unmatched rows.
         */
        switch (excessRowMode) {

            case 1:
                for (int i = fixedSheet.getPhysicalNumberOfRows(); i < customSheet.getPhysicalNumberOfRows(); i++) {
                    unmatchedRows.add(customSheet.getRow(i));
                }
                break;

            case 2:
                for (int i = customSheet.getPhysicalNumberOfRows(); i < fixedSheet.getPhysicalNumberOfRows(); i++) {
                    unmatchedRows.add(fixedSheet.getRow(i));
                }
                break;

        }

        if (unmatchedRows.size() > 1) {
            return generateWorkbookFromRows(unmatchedRows, FileHelper.EXTENSION_XLSX);
        } else {
            throw new IllegalArgumentException("No difference found!");
        }

    }

    private static Workbook generateWorkbookFromRows(List<Row> rows, int WORKBOOK_TYPE) throws IllegalArgumentException {

        Workbook diffBook;
        Sheet diffSheet;

        switch (WORKBOOK_TYPE) {

            case FileHelper.EXTENSION_XLSX:
                diffBook = new XSSFWorkbook();
                break;

            case FileHelper.EXTENSION_XLS:
                diffBook = new HSSFWorkbook();
                break;

            default:
                throw new IllegalArgumentException("Incorrect file type!");
        }

        FormulaEvaluator evaluator = diffBook.getCreationHelper().createFormulaEvaluator();
        diffSheet = diffBook.createSheet();

        for (int i = 0; i < rows.size(); i++) {
            Row dataRow = diffSheet.createRow(i);
            Row r = rows.get(i);

            for (int j = 0; j < r.getPhysicalNumberOfCells(); j++) {
                Cell c = r.getCell(j);

                switch (evaluator.evaluateInCell(c).getCellType()) {

                    case STRING:
                        dataRow.createCell(j).setCellValue(c.getStringCellValue());
                        break;

                    case NUMERIC:
                        dataRow.createCell(j).setCellValue(c.getNumericCellValue());
                        break;

                    case BOOLEAN:
                        dataRow.createCell(j).setCellValue(c.getBooleanCellValue());
                        break;
                    default:
                    	break;
                }

            }
        }

        return diffBook;
    }

    private static Workbook evaluateXLS(List<CompareModel> compareColumnList, Workbook fixedFile, Workbook customFile) {

        return null;
    }

    public static Workbook getWorkbook(File file, int FILE_TYPE) throws IOException, InvalidFormatException {

        switch (FILE_TYPE) {
            case FileHelper.EXTENSION_XLS:
                return new HSSFWorkbook(new FileInputStream(file));

            case FileHelper.EXTENSION_XLSX:
                return new XSSFWorkbook(file);

            default:
                throw new InvalidFormatException("Incorrect file type argument!");
        }
    }

    public static List<ExcelColumn> processExcelColumns(Workbook workbook) throws IOException, InvalidFormatException {

        try {
            Sheet dataSheet = workbook.getSheetAt(0);
            Row row = dataSheet.getRow(0);

            List<ExcelColumn> columns = new ArrayList<>();

            for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                Cell c = row.getCell(i);

                columns.add(new ExcelColumn(c.getStringCellValue(), i + 1));
            }
            return columns;
        } catch (NullPointerException e) {
            throw new InvalidFormatException("Error! Unable to read file.");
        }

    }

    public static List<ExcelColumn> processExcelColumns(File file) throws IOException, InvalidFormatException {

        Workbook workbook;
        Sheet dataSheet;
        Row row;

        try {
            switch (FileHelper.getExtension(file)) {

                case "xlsx":
                    workbook = new XSSFWorkbook(file);
                    break;

                case "xls":
                    workbook = new HSSFWorkbook(new FileInputStream(file));
                    break;

                default:
                    throw new InvalidFormatException("Invalid file! Only Excel files (.xls or .xlsx) are supported.");

            }
        } catch (OfficeXmlFileException exc) {
            workbook = new XSSFWorkbook(file);
        }
        catch (Exception exc) {
            throw new InvalidFormatException("Unable to read file. Please try again with different format.");
        }

        try {
            dataSheet = workbook.getSheetAt(0);
            row = dataSheet.getRow(0);

            List<ExcelColumn> columns = new ArrayList<>();

            for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                Cell c = row.getCell(i);

                columns.add(new ExcelColumn(c.getStringCellValue(), i + 1));
            }
            return columns;
        } catch (NullPointerException e) {
            throw new InvalidFormatException("Error! Unable to read file.");
        }

    }

    public static boolean isColumnCountEqual(File w1, File w2) throws IOException, InvalidFormatException {

        if (processExcelColumns(w1).size() == processExcelColumns(w2).size()) {
            return true;
        }

        return false;
    }

}
