/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author mjkulkarni
 */
public class FileHelper {

    public static final int EXTENSION_XLS = 1;
    public static final int EXTENSION_XLSX = 2;

    public static File chooseFile(Window window) throws FileNotFoundException {

        FileChooser fileChooser = new FileChooser();
        File file = fileChooser.showOpenDialog(window);

        if (file == null) {
            throw new FileNotFoundException("No file selected or file not found!");
        }

        return file;

    }

    public static String getExtension(File file) {
        return FilenameUtils.getExtension(file.getName());
    }

    public static void saveFile(File file, int FILE_TYPE) throws Exception {

    }

    public static void saveWorkbook(Workbook workbook, int FILE_TYPE, Window window)
            throws FileNotFoundException, IOException, IllegalArgumentException {

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter;

        switch (FILE_TYPE) {
            case EXTENSION_XLSX:
                extFilter = new FileChooser.ExtensionFilter("Excel Workbook (*.xlsx)", "*.xlsx");
                break;

            case EXTENSION_XLS:
                extFilter = new FileChooser.ExtensionFilter("Excel 97-2003 Workbook (*.xls)", "*.xls");
                break;

            default:
                throw new IllegalArgumentException("Incorrect file type!");
        }

        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showSaveDialog(window);
        if(file == null) {
            throw new IllegalArgumentException("No file selected!");
        }
        try (FileOutputStream out = new FileOutputStream(file)) {
            workbook.write(out);
        } catch (Exception e) {
            throw e;
        }

    }

}
