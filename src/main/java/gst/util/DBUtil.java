/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Hetarth
 */
public class DBUtil {

    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static Connection CONNECTION = null;
    private static final String CONNSTR = "jdbc:mysql://localhost:3306/db_fxdemo";
    private static Statement stmt = null;

    public static void main(String[] args) {
        dbConnect();
    }

    public static void dbConnect() {
        try {
            Class.forName(JDBC_DRIVER);
        } catch (ClassNotFoundException ce) {
            System.out.println("Db driver not found");
        }
        try {
            CONNECTION = DriverManager.getConnection(CONNSTR, "root", "amcs_MySQL@123");
        } catch (SQLException e) {
            System.out.println("connection faild ! please check");
        }
    }

    public static void dbDisconnect() {
        try {
            if (stmt != null) {
                stmt.close();
            }
            if (CONNECTION != null && !CONNECTION.isClosed()) {
                CONNECTION.close();
            }
        } catch (SQLException e) {
            System.out.println("connection can't be close");
        }
    }

    public static void dbExecuteQuery(String sqlStmt) {
        try {
            dbConnect();
            stmt = CONNECTION.createStatement();
            stmt.executeUpdate(sqlStmt);
        } catch (SQLException e) {
            System.out.println("error occured in dbExecute query");
        } finally {
            dbDisconnect();
        }
    }

    public static ResultSet dbExecuteSelect(String selectQuery) {
        ResultSet rs = null;
        try {
            dbConnect();
            stmt = CONNECTION.createStatement();
            rs = stmt.executeQuery(selectQuery);
            return rs;
         } catch (SQLException e) {
            System.out.println("error occured in dbExecute query");
            return null;
        } 
    }
}
