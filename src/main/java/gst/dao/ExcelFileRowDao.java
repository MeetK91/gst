/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.dao;

import java.sql.ResultSet;

import gst.util.DBUtil;

/**
 *
 * @author mjkulkarni
 */
public class ExcelFileRowDao {

    public static void insert(Object... param) {
        String sql = "insert into tbl_excel_file_row(file_hash,sr_no,data) values ('" 
                + param[0] + "'," 
                + param[1] + "," 
                + param[2] + 
                ")";
        DBUtil.dbExecuteQuery(sql);
    }

    public static ResultSet fetchAll() {
        String sql = "select * from tbl_excel_file_row";
        return DBUtil.dbExecuteSelect(sql);
    }

}