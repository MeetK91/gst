/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gst.model.Client;
import gst.util.DBUtil;

/**
 *
 * @author Hetarth
 */
public class ClientDao {

    public static void insert(Client client) {
        String sql = "insert into tbl_client(client_name,is_active) "
                + "values ("
                + "'" + client.getClientName() + "',"
                + client.isIsActive()
                + ")";
        DBUtil.dbExecuteQuery(sql);
    }

    public static List<Client> fetchAll() throws SQLException {

        String sql = "select * from tbl_client";
        ResultSet rs = DBUtil.dbExecuteSelect(sql);

        List<Client> listClient = new ArrayList<>();
        
        while (rs.next()) {
            Client client = new Client();
            client.setClientId(rs.getInt("client_id"));
            client.setClientName(rs.getString("client_name"));
            client.setIsActive(rs.getBoolean("is_active"));
            listClient.add(client);
        }

        return listClient;
    }

}
