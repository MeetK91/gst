/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import gst.model.ExcelFile;
import gst.util.DBUtil;

/**
 *
 * @author mjkulkarni
 */
public class ExcelFileDao {

    public static void insert(ExcelFile file) {
        String sql = "insert into tbl_excel_file(file_hash,file_name,absolute_path,column_count,is_custom, client_id) "
                + "values ("
                + "'" + file.getFileHash() + "',"
                + "'" + file.getFileName() + "',"
                + "'" + file.getAbsolutePath() + "',"
                + file.getColumnCount() + ","
                + file.isIsCustom() + ","
                + "'" + file.getClientId() + "'"
                + ")";
        DBUtil.dbExecuteQuery(sql);
    }

    public static List<ExcelFile> fetchAll() throws SQLException {
        String sql = "select * from tbl_excel_file";

        ResultSet rs = DBUtil.dbExecuteSelect(sql);

        List<ExcelFile> fileList = new ArrayList<>();

        while (rs.next()) {

            ExcelFile file = new ExcelFile();

            file.setClientId(rs.getInt("client_id"));
            file.setColumnCount(rs.getInt("column_count"));
            file.setFileHash(rs.getString("file_hash"));
            file.setAbsolutePath(rs.getString("absolute_path"));
            file.setFileName(rs.getString("file_name"));
            file.setIsCustom(rs.getBoolean("is_custom"));

            fileList.add(file);
        }

        return fileList;
    }

}