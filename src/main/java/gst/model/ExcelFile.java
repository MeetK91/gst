/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.model;

import gst.util.JsonHelper;

/**
 *
 * @author mjkulkarni
 */
public class ExcelFile {

    private String fileHash;

    private String fileName;

    private String absolutePath;

    private int columnCount;
    
    private int clientId;

    private boolean isCustom;

    public ExcelFile() {
    }

    public ExcelFile(String fileHash, String fileName, String absolutePath, int columnCount, int clientId, boolean isCustom) {
        this.fileHash = fileHash;
        this.fileName = fileName;
        this.absolutePath = absolutePath;
        this.columnCount = columnCount;
        this.clientId = clientId;
        this.isCustom = isCustom;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public int getColumnCount() {
        return columnCount;
    }

    public void setColumnCount(int columnCount) {
        this.columnCount = columnCount;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public boolean isIsCustom() {
        return isCustom;
    }

    public void setIsCustom(boolean isCustom) {
        this.isCustom = isCustom;
    }
    
    @Override
    public String toString() {
        try {
            return JsonHelper.getObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return null;
        }
    }

}