/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.model;

import gst.util.JsonHelper;

/**
 *
 * @author Hetarth
 */
public class Employee {
 
    @Override
    public String toString() {
        try {
            return JsonHelper.getObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return null;
        }
    }
}
