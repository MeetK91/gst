/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.model;

import javafx.beans.property.SimpleBooleanProperty;

/**
 *
 * @author mjkulkarni
 */
public class FinalTableModel {

    private String fixedColumn;
    private String customColumn;
    private SimpleBooleanProperty compare;

    public String getFixedColumn() {
        return fixedColumn;
    }

    public void setFixedColumn(String fixedColumn) {
        this.fixedColumn = fixedColumn;
    }

    public String getCustomColumn() {
        return customColumn;
    }

    public void setCustomColumn(String customColumn) {
        this.customColumn = customColumn;
    }

    public SimpleBooleanProperty compareProperty(){
        return compare;
    }
    
    public SimpleBooleanProperty getCompare() {
        return compare;
    }

    public void setCompare(SimpleBooleanProperty compare) {
        this.compare = compare;
    }

}