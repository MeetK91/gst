/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.model;

import gst.util.JsonHelper;

/**
 *
 * @author mjkulkarni
 */
public class ExcelColumn {
    
    private String columnName;
    private int columnIndex;
    private boolean isCompareColumn;

    public ExcelColumn(String columnName, int columnIndex) {
        this.columnName = columnName;
        this.columnIndex = columnIndex;
        this.isCompareColumn = false;
    }

    public ExcelColumn(String columnName, int columnIndex, boolean isCompareColumn) {
        this.columnName = columnName;
        this.columnIndex = columnIndex;
        this.isCompareColumn = isCompareColumn;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public void setColumnIndex(int columnIndex) {
        this.columnIndex = columnIndex;
    }

    public boolean isIsCompareColumn() {
        return isCompareColumn;
    }

    public void setIsCompareColumn(boolean isCompareColumn) {
        this.isCompareColumn = isCompareColumn;
    }
    
    @Override
    public String toString() {
        try {
            return JsonHelper.getObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return null;
        }
    }
}