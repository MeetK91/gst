/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.model;

import gst.util.JsonHelper;

/**
 *
 * @author mjkulkarni
 */
public class ExcelFileRow {

    private String fileHash;
    private int srNo;
    private String data;

    public ExcelFileRow(String fileHash, int srNo, String data) {
        this.fileHash = fileHash;
        this.srNo = srNo;
        this.data = data;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public int getSrNo() {
        return srNo;
    }

    public void setSrNo(int srNo) {
        this.srNo = srNo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        try {
            return JsonHelper.getObjectMapper().writeValueAsString(this);
        } catch (Exception e) {
            return null;
        }
    }
}
