/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gst.model;

/**
 *
 * @author mjkulkarni
 */
public class CompareModel {
    
    private int fixedIndex;
    private int customIndex;
    private boolean compare;

    public int getFixedIndex() {
        return fixedIndex;
    }

    public void setFixedIndex(int fixedIndex) {
        this.fixedIndex = fixedIndex;
    }

    public int getCustomIndex() {
        return customIndex;
    }

    public void setCustomIndex(int customIndex) {
        this.customIndex = customIndex;
    }

    public boolean isCompare() {
        return compare;
    }

    public void setCompare(boolean compare) {
        this.compare = compare;
    }

    public CompareModel() {
    }

    public CompareModel(int fixed_index, int custom_index, boolean compare) {
        this.fixedIndex = fixed_index;
        this.customIndex = custom_index;
        this.compare = compare;
    }
    
    @Override
    public String toString(){
        String s = "["+
                    "Fixed Index: " + this.fixedIndex + ", " +
                    "Custom Index: " + this.customIndex + ", " +
                    "Compare: " + this.compare 
                    + "]\n";
        return s;
    }
}